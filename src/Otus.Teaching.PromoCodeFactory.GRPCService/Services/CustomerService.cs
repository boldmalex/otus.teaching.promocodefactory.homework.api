using Grpc.Core;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.GRPCService.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.GRPCService
{
    public class CustomerService : Customers.CustomersBase
    {
        private readonly ILogger<CustomerService> _logger;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;


        public CustomerService(ILogger<CustomerService> logger,
                               IRepository<Customer> customerRepository,
                               IRepository<Preference> preferenceRepository)   
        {
            _logger = logger;
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }


        public override async Task<ResponseCustomerList> GetCustomers(RequestEmptyModel request, ServerCallContext context)
        {
            var customers = await _customerRepository.GetAllAsync();

            var response = new ResponseCustomerList();

            response.CustomersShort.AddRange(
                customers.Select(x => new ResponseCustomerShort()
                {
                    Id = x.Id.ToString(),
                    Email = x.Email,
                    FirstName = x.FirstName,
                    LastName = x.LastName
                }));

            return response;
        }


        public override async Task<ResponseCustomer> GetCustomer(RequestIdModel request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            var response = new ResponseCustomer()
            {
                Id = customer.Id.ToString(),
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
            };
            
            response.Preferences.AddRange(customer.Preferences.Select(x=> new ResponsePreference()
            {
                Id = x.Preference.Id.ToString(),
                Name = x.Preference.Name
            }));

            return response;
        }


        public override async Task<ResponseCustomer> CreateCustomer(RequestCreateCustomer request, ServerCallContext context)
        {
            //�������� ������������ �� �� � ��������� ������� ������
            var preferences = await _preferenceRepository
                .GetRangeByIdsAsync(request.Customer.PreferenceIds.Select(x=>Guid.Parse(x)).ToList());

            Customer customer = CustomerMapper.MapFromModel(request.Customer, preferences);

            await _customerRepository.AddAsync(customer);

            var response = new ResponseCustomer()
            {
                Id = customer.Id.ToString(),
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
            };

            response.Preferences.AddRange(customer.Preferences.Select(x => new ResponsePreference()
            {
                Id = x.Preference.Id.ToString(),
                Name = x.Preference.Name
            }));

            return response;
        }


        public override async Task<ResponseResult> EditCustomer(RequestEditCustomer request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id.Id));

            if (customer == null)
                return new ResponseResult() { Result = EnumResult.Error };

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds.Select(x=>Guid.Parse(x)).ToList());

            CustomerMapper.MapFromModel(request, preferences, customer);

            await _customerRepository.UpdateAsync(customer);

            return new ResponseResult() { Result = EnumResult.Ok };
        }


        public override async Task<ResponseResult> DeleteCustomer(RequestIdModel request, ServerCallContext context)
        {
            var customer = await _customerRepository.GetByIdAsync(Guid.Parse(request.Id));

            if (customer == null)
                return new ResponseResult() { Result = EnumResult.Error };

            await _customerRepository.DeleteAsync(customer);

            return new ResponseResult() { Result = EnumResult.Ok };
        }
    }
}
