﻿using Grpc.Net.Client;
using Otus.Teaching.PromoCodeFactory.GRPCClient;
using System;


namespace Otus.Teaching.PromoCodeFactory.GRPCServiceClient
{
    class Program
    {
        static void Main(string[] args)
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");

            var client = new Customers.CustomersClient(channel);



            Console.WriteLine("Нажмите что-нить, чтобы получить клиентов");
            Console.ReadKey();
            Console.WriteLine();

            var clientsResponse = client.GetCustomers(new RequestEmptyModel());

            foreach (var customer in clientsResponse.CustomersShort)
            {
                Console.WriteLine($"Клиент id: {customer.Id}, name: {customer.FirstName} {customer.LastName}");
            }



            Console.WriteLine("Укажите id клиента, чтобы получить его данные");
            var id = Console.ReadLine();

            var clientResponse = client.GetCustomer(new RequestIdModel() { Id = id });

            Console.WriteLine($"Клиент id: {clientResponse.Id}, name: {clientResponse.FirstName} {clientResponse.LastName}");
            Console.WriteLine("Предпочтения:");
            foreach (var preference in clientResponse.Preferences)
            {
                Console.WriteLine($"id: {preference.Id}, name: {preference.Name}");
            }

            Console.ReadKey();           

        }
    }
}
