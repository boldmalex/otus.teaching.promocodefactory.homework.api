﻿using CustomerGraphQLReader;
using HotChocolate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.GraphQL
{
    public class Query
    {
        public Task<ICollection<CustomerShortResponse>> GetCustomersAsync(
            [Service] CustomerGraphQLService service,
            CancellationToken cancellationToken)
        {
            return service.GetCustomersAsync(cancellationToken);
        }

        public Task<CustomerResponse> GetCustomerByIdAsync(
            [Service] CustomerGraphQLService service,
            Guid id,
            CancellationToken cancellationToken)
        {
            return service.GetCustomerAsync(id, cancellationToken);
        }
    }
}
